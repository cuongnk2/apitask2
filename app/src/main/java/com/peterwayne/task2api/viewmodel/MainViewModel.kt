package com.peterwayne.task2api.viewmodel

import android.app.DownloadManager
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.peterwayne.task2api.Util
import com.peterwayne.task2api.api.APIService
import com.peterwayne.task2api.model.Birthday
import com.peterwayne.task2api.model.Category
import com.peterwayne.task2api.model.PhotoFrame
import com.peterwayne.task2api.model.Song
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainViewModel: ViewModel() {
    private val _categoryList = MutableLiveData<List<Category>>()
    val categoryList: LiveData<List<Category>> = _categoryList
    private val _photoFrameList = MutableLiveData<List<PhotoFrame>>()
    val photoFrameList: LiveData<List<PhotoFrame>> = _photoFrameList
    private val _songList = MutableLiveData<List<Song>>()
    val songList: LiveData<List<Song>> = _songList
    var downloadID :Long? = null
    fun callAPI()
    {
        APIService.apiService.getBirthdayData().enqueue(object : Callback<Birthday>
        {
            override fun onResponse(call: Call<Birthday>, response: Response<Birthday>) {
                val birthday = response.body()!!
                generateCategoryList(birthday)
                generatePhotoFrameList(birthday)
            }
            override fun onFailure(call: Call<Birthday>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun generatePhotoFrameList(birthday: Birthday) {
        val frameList = mutableListOf<PhotoFrame>()
        for(category in birthday.listPhotoFrames!!) {
                category.listFrame = mutableListOf()
                val define = category.defines!![0]
                for(index in define.start!!..define.end!!)
                {
                    category.listFrame.add(PhotoFrame(birthday.start_link!!,category.folder!!,index))
                }
                frameList.addAll(category.listFrame)
            }
        _photoFrameList.postValue(frameList)

    }
    private fun generateCategoryList(birthday: Birthday) {

        birthday.listPhotoFrames!!.map {
                category -> category.start_link = birthday.start_link!!
        }
        _categoryList.postValue(birthday.listPhotoFrames)
    }
     fun getAllSongs(contentResolver: ContentResolver)
    {
        val songs: MutableList<Song> = mutableListOf()
        val selection = MediaStore.Audio.Media.IS_MUSIC + " != 0"
        val projection = arrayOf(
            MediaStore.Audio.Media.TITLE,
            MediaStore.Audio.Media.ARTIST,
            MediaStore.Audio.Media.DATA,
            MediaStore.Audio.Media.DURATION,
        )

        val cursor = contentResolver.query(
            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
            projection,
            selection,
            null,
            null
        )
        if (cursor!=null)
        {
            while (cursor.moveToNext()) {
                songs.add(Song(cursor.getString(0), cursor.getString(1), cursor.getString(2)))
            }
        }
        _songList.postValue(songs)
    }
    fun downloadImage(context : Context, photoFrame: PhotoFrame) {
        if (!Util.isFileExisted(photoFrame.getPhotoName())) {
            Log.d("PhotoName", photoFrame.getPhotoName())
            val downloadManager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            val downloadUri = Uri.parse(photoFrame.getPhotoUrl())
            val request = DownloadManager.Request(downloadUri).apply {
                setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
                    .setAllowedOverRoaming(false)
                    .setTitle(
                        photoFrame.getPhotoUrl()
                            .substring(photoFrame.getPhotoUrl().lastIndexOf("/") + 1)
                    )
                    .setDescription("")
                    .setDestinationInExternalPublicDir(
                        Environment.DIRECTORY_PICTURES,
                        "MyPic/${photoFrame.getPhotoName()}"
                    )
            }
            downloadID = downloadManager.enqueue(request)
            Log.d("downloadId", downloadID.toString())
            val intent = Intent(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
            intent.putExtra(DownloadManager.EXTRA_DOWNLOAD_ID, downloadID)
            context.sendBroadcast(intent)
        }
    }

}
package com.peterwayne.task2api.model

data class Category(val name:String?,
                    val name_vi:String?,
                    val folder:String?,
                    val icon:String?,
                    val cover:String?,
                    val totalImage:Int?,
                    val lock:Boolean?,
                    val openPacketName:String?,
                    val defines : List<Define>?,
                    var start_link: String,
                    var listFrame : MutableList<PhotoFrame>)
package com.peterwayne.task2api.model

import java.io.Serializable

data class PhotoFrame(val startLink: String,val folder :String , val index : Int) : Serializable {
    fun getPhotoName():String
    {
        return getPhotoUrl().substring(getPhotoUrl().lastIndexOf("/") + 1)
    }

    fun getPhotoUrl():String {
        return "${startLink}${folder}/${folder}_frame_${index}.png"
    }
}
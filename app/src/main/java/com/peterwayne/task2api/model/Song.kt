package com.peterwayne.task2api.model

data class Song(val title : String, val artist: String?, val uri : String)

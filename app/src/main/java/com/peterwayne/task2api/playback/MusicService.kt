package com.peterwayne.task2api.playback

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log

class MusicService : Service() {
    private val mIBinder = LocalBinder()
    var musicPlayer : MusicPlayer? = null
        private set
    var musicNotificationManager : MusicNotificationManager? = null
        private set
    var isRestoredFromPause = false

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_NOT_STICKY
    }

    override fun onDestroy() {
        musicPlayer!!.registerNotificationActionsReceiver(false)
        musicNotificationManager = null
        musicPlayer!!.release()
        super.onDestroy()
    }
    override fun onBind(p0: Intent?): IBinder? {
        if (musicPlayer == null) {
            musicPlayer = MusicPlayer(this)
            musicNotificationManager = MusicNotificationManager(this)
            musicPlayer!!.registerNotificationActionsReceiver(true)
        }
        return mIBinder
    }
    inner class LocalBinder : Binder(){
        val instance : MusicService
            get() =this@MusicService
    }
}
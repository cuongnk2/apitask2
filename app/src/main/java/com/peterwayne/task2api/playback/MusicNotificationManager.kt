package com.peterwayne.task2api.playback
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_UPDATE_CURRENT
import android.content.Context
import android.content.Intent
import android.media.session.MediaSessionManager
import android.os.Build
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.peterwayne.task2api.MainActivity
import com.peterwayne.task2api.R
import com.peterwayne.task2api.Util
import com.peterwayne.task2api.model.Song


class MusicNotificationManager constructor(private val musicService: MusicService) {
    private val CHANNEL_ID = "MUSIC_CHANNEL"
    private val REQUEST_CODE = 100
    private val context: Context = musicService.baseContext
    val notificationManager: NotificationManager =
        musicService.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    var notificationBuilder: NotificationCompat.Builder? = null
    private var mediaSession: MediaSessionCompat? = null
    private var mediaSessionManager: MediaSessionManager? = null
    private var transportControls: MediaControllerCompat.TransportControls? = null

    companion object {
        const val NOTIFICATION_ID = 101
        internal const val PLAY_PAUSE_ACTION = "action.PLAYPAUSE"
        internal const val NEXT_ACTION = "action.NEXT"
        internal const val PREV_ACTION = "action.PREV"
    }

    fun createNotification(): Notification {
        val song = musicService.musicPlayer!!.getCurrentSong()
        notificationBuilder = NotificationCompat.Builder(musicService, CHANNEL_ID)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        }

        val openPlayerIntent = Intent(musicService, MainActivity::class.java)
        openPlayerIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        val contentIntent = PendingIntent.getActivity(
            musicService, REQUEST_CODE,
            openPlayerIntent, 0
        )

        val artist = song!!.artist
        val songTitle = song.title

        initMediaSession(song)

        notificationBuilder!!
            .setShowWhen(false)
            .setSound(null)
            .setSmallIcon(R.drawable.player)
            .setLargeIcon(Util.songArt(song.uri, musicService.baseContext))
            .setContentTitle(songTitle)
            .setContentText(artist)
            .setContentIntent(contentIntent)
            .addAction(notificationAction(PREV_ACTION))
            .addAction(notificationAction(PLAY_PAUSE_ACTION))
            .addAction(notificationAction(NEXT_ACTION))
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)

        notificationBuilder!!.setStyle(
            androidx.media.app.NotificationCompat.MediaStyle()
                .setMediaSession(mediaSession!!.sessionToken)
                .setShowActionsInCompactView(0, 1, 2)
        )
        return notificationBuilder!!.build()
    }

    private fun initMediaSession(song: Song) {
        mediaSessionManager =
            context.getSystemService(Context.MEDIA_SESSION_SERVICE) as MediaSessionManager
        mediaSession = MediaSessionCompat(context, "AudioPlayer")
        transportControls = mediaSession!!.controller.transportControls
        mediaSession!!.isActive = true
        mediaSession!!.setFlags(MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS)
        updateMetaData(song)
    }

    private fun updateMetaData(song: Song) {
        mediaSession!!.setMetadata(
            MediaMetadataCompat.Builder()
                .putBitmap(
                    MediaMetadataCompat.METADATA_KEY_ALBUM_ART,
                    Util.songArt(song.uri, context)
                )
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, song.artist)
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, song.title)
                .build()
        )
    }

    private fun notificationAction(action: String): NotificationCompat.Action {
        val icon: Int

        when (action) {
            PREV_ACTION -> icon = R.drawable.ic_previous
            PLAY_PAUSE_ACTION ->
                icon =
                    if (musicService.musicPlayer?.getState() != PlaybackInfoListener.PAUSED)
                        R.drawable.ic_pause
                    else
                        R.drawable.ic_play
            NEXT_ACTION -> icon = R.drawable.ic_next
            else -> icon = R.drawable.ic_previous
        }
        return NotificationCompat.Action.Builder(icon, action, playerAction(action)).build()
    }
    private fun playerAction(action: String): PendingIntent {
        val intent = Intent()
        intent.action = action
        return PendingIntent.getBroadcast(
            musicService,
            REQUEST_CODE,
            intent,
            FLAG_UPDATE_CURRENT
        )
    }

    @RequiresApi(26)
    private fun createNotificationChannel() {

        if (notificationManager.getNotificationChannel(CHANNEL_ID) == null) {
            val notificationChannel = NotificationChannel(
                CHANNEL_ID,
                musicService.getString(R.string.app_name),
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationChannel.setSound(null, null)
            notificationChannel.description = musicService.getString(R.string.app_name)
            notificationChannel.enableLights(false)
            notificationChannel.enableVibration(false)
            notificationChannel.setShowBadge(false)
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }
}
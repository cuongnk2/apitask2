package com.peterwayne.task2api.playback

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.os.PowerManager
import com.peterwayne.task2api.model.Song
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

class MusicPlayer(private val musicService: MusicService?) : PlayerAdapter,
    MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener {
    private val context: Context = musicService!!.applicationContext
    private var mediaPlayer: MediaPlayer? = null
    private var mPlaybackInfoListener: PlaybackInfoListener? = null
    private var mExecutor: ScheduledExecutorService? = null
    private var sliderPositionUpdateTask: Runnable? = null
    private var currentSong: Song? = null
    private var songList: List<Song>? = null
    private var replay = false

    //State
    private var mState: Int = PlaybackInfoListener.INVALID
    private var mNotificationActionsReceiver: NotificationReceiver? = null
    private var mMusicNotificationManager: MusicNotificationManager? = null


    override fun initMediaPlayer() {
        try {
            if (mediaPlayer != null) {
                mediaPlayer!!.reset()
            } else {
                mediaPlayer = MediaPlayer()
                mediaPlayer!!.setOnPreparedListener(this)
                mediaPlayer!!.setOnCompletionListener(this)
                mediaPlayer!!.setWakeMode(context, PowerManager.PARTIAL_WAKE_LOCK)
                mediaPlayer!!.setAudioAttributes(
                    AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .build()
                )
                mMusicNotificationManager = musicService!!.musicNotificationManager
            }
            mediaPlayer!!.setDataSource(currentSong!!.uri)
            mediaPlayer!!.prepare()
        } catch (e: Exception) {
            e.printStackTrace()
            skip(true)
        }
    }

    override fun getMediaPlayer(): MediaPlayer? {
        return mediaPlayer
    }

    override fun isMediaPlayer(): Boolean {
        return mediaPlayer != null
    }

    override fun isPlaying(): Boolean {
        return isMediaPlayer() && mediaPlayer!!.isPlaying
    }

    override fun reset() {
        replay = !replay
    }

    override fun isReset(): Boolean {
        return replay
    }

    override fun getCurrentSong(): Song? {
        return currentSong
    }

    override fun setCurrentSong(song: Song, songs: List<Song>) {
        currentSong = song
        songList = songs
    }

    override fun getState(): Int {
        return mState
    }

    override fun getPlayerPosition(): Int {
        return mediaPlayer!!.currentPosition
    }

    override fun release() {
        if (isMediaPlayer()) {
            mediaPlayer!!.release()
            mediaPlayer = null
            unregisterActionsReceiver()
        }
    }

    override fun resumeOrPause() {
        if (isPlaying()) {
            pauseMediaPlayer()
        } else {
            resumeMediaPlayer()
        }
    }


    override fun instantReset() {
        if (isMediaPlayer()) {
            if (mediaPlayer!!.currentPosition < 5000) {
                skip(false)
            } else {
                resetSong()
            }
        }
    }

    override fun skip(isNext: Boolean) {
        val currentIndex = songList!!.indexOf(currentSong)
        val index: Int
        try {
            index = if (isNext) currentIndex + 1 else currentIndex - 1
            currentSong = songList!![index]
        } catch (e: IndexOutOfBoundsException) {
            currentSong = if (currentIndex != 0) songList!![0] else songList!![songList!!.size - 1]
            e.printStackTrace()
        }
        initMediaPlayer()
    }


    override fun seekTo(position: Int) {
        if (isMediaPlayer()) {
            mediaPlayer!!.seekTo(position)
        }
    }

    override fun setPlaybackInfoListener(playbackInfoListener: PlaybackInfoListener) {
        mPlaybackInfoListener = playbackInfoListener
    }

    override fun registerNotificationActionsReceiver(isRegister: Boolean) {
        if (isRegister) {
            registerActionsReceiver()
        } else {
            unregisterActionsReceiver()
        }
    }

    override fun onPauseActivity() {
        stopUpdatingCallbackWithPosition()
    }

    private fun stopUpdatingCallbackWithPosition() {
        if (mExecutor != null) {
            mExecutor!!.shutdownNow()
            mExecutor = null
            sliderPositionUpdateTask = null
        }
    }

    override fun onResumeActivity() {
        startUpdatingCallbackWithPosition()
    }

    override fun getDuration() : Float{
        return if(isMediaPlayer()) mediaPlayer!!.duration.toFloat()
        else 0f
    }

    private fun startUpdatingCallbackWithPosition() {
        if (mExecutor == null) {
            mExecutor = Executors.newSingleThreadScheduledExecutor()
        }
        if (sliderPositionUpdateTask == null) {
            sliderPositionUpdateTask = Runnable {
                updateProgressCallbackTask()
            }
        }
        mExecutor!!.scheduleAtFixedRate(
            sliderPositionUpdateTask,
            0,
            1000,
            TimeUnit.MILLISECONDS
        )
    }

    private fun updateProgressCallbackTask() {
        if (isPlaying()) {
            val currentPosition = mediaPlayer!!.currentPosition
            if (mPlaybackInfoListener != null) {
                mPlaybackInfoListener!!.onPositionChanged(currentPosition)
            }
        }
    }

    override fun onCompletion(mediaPlayer: MediaPlayer) {
        if (mPlaybackInfoListener != null) {
            mPlaybackInfoListener!!.onStateChanged(PlaybackInfoListener.COMPLETED)
            mPlaybackInfoListener!!.onPlaybackCompleted()
        }
        if (replay) {
            if (isMediaPlayer()) {
                resetSong()
            }
            replay = false
        } else {
            skip(true)
        }
    }

    override fun onPrepared(p0: MediaPlayer?) {
        startUpdatingCallbackWithPosition()
        setStatus(PlaybackInfoListener.PLAYING)
    }

    private fun resumeMediaPlayer() {
        if (!isPlaying()) {
            mediaPlayer!!.start()
            setStatus(PlaybackInfoListener.RESUMED)
            musicService!!.startForeground(
                MusicNotificationManager.NOTIFICATION_ID,
                mMusicNotificationManager!!.createNotification()
            )
        }
    }

    private fun pauseMediaPlayer() {
        setStatus(PlaybackInfoListener.PAUSED)
        mediaPlayer!!.pause()
        musicService!!.stopForeground(false)
        mMusicNotificationManager!!.notificationManager.notify(
            MusicNotificationManager.NOTIFICATION_ID,
            mMusicNotificationManager!!.createNotification()
        )
    }

    private fun resetSong() {
        mediaPlayer!!.seekTo(0)
        mediaPlayer!!.start()
        setStatus(PlaybackInfoListener.PLAYING)
    }

    private fun setStatus( state: Int) {
        mState = state
        if (mPlaybackInfoListener != null) {
            mPlaybackInfoListener!!.onStateChanged(state)
        }
    }

    private inner class NotificationReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action

            if (action != null) {
                when (action) {
                    MusicNotificationManager.PREV_ACTION -> instantReset()
                    MusicNotificationManager.PLAY_PAUSE_ACTION -> resumeOrPause()
                    MusicNotificationManager.NEXT_ACTION -> skip(true)
                }
            }
        }
    }
    private fun registerActionsReceiver() {
        mNotificationActionsReceiver = NotificationReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction(MusicNotificationManager.PREV_ACTION)
        intentFilter.addAction(MusicNotificationManager.PLAY_PAUSE_ACTION)
        intentFilter.addAction(MusicNotificationManager.NEXT_ACTION)

        musicService!!.registerReceiver(mNotificationActionsReceiver, intentFilter)
    }
    private fun unregisterActionsReceiver() {
        if (musicService != null && mNotificationActionsReceiver != null) {
            try {
                musicService!!.unregisterReceiver(mNotificationActionsReceiver)
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            }
        }
    }
}
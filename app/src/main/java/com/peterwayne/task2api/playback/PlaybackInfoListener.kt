package com.peterwayne.task2api.playback

abstract class PlaybackInfoListener {
    open fun onPositionChanged(position: Int) {}

    open fun onStateChanged(state: Int) {}

    open fun onPlaybackCompleted() {}

    companion object {
        const val INVALID = -1
        const val PLAYING = 0
        const val PAUSED = 1
        const val COMPLETED = 2
        const val RESUMED = 3
    }
}
package com.peterwayne.task2api.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import androidx.core.view.marginLeft
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.shape.CornerFamily
import com.peterwayne.task2api.Util
import com.peterwayne.task2api.databinding.ItemPhotoFrameBinding
import com.peterwayne.task2api.model.PhotoFrame

class PhotoFrameAdapter(private var adapterListener : OnPhotoFrameClickListener?) : RecyclerView.Adapter<PhotoFrameAdapter.ViewHolder>() {
    var photoFrameList = listOf<PhotoFrame>()
    set(value) {
        field = value
        notifyDataSetChanged()
    }
    var selectedPosition = -1
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemPhotoFrameBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(photoFrameList[position])
    }

    override fun getItemCount(): Int {
        return photoFrameList.size
    }


    inner class ViewHolder(private val itemBinding: ItemPhotoFrameBinding) : RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(photoFrame: PhotoFrame)
        {
            if(Util.isFileExisted(photoFrame.getPhotoName()))
            {
                itemBinding.imgDownload.visibility = View.GONE
            }
            itemBinding.imgDownload.shapeAppearanceModel = itemBinding.imgDownload.shapeAppearanceModel.toBuilder()
                .setBottomLeftCorner(CornerFamily.ROUNDED,8f)
                .build()
            Glide.with(itemBinding.root)
                .load(photoFrame.getPhotoUrl())
                .apply(RequestOptions().override(40, 40))
                .fitCenter()
                .into(itemBinding.imgFrame)
            if(adapterPosition%6==0)
            {
                itemBinding.imgFrame.shapeAppearanceModel = itemBinding.imgFrame.shapeAppearanceModel.toBuilder()
                    .setTopLeftCorner(CornerFamily.ROUNDED,18f)
                    .setBottomLeftCorner(CornerFamily.ROUNDED,18f)
                    .build()
                itemBinding.imgFrameSelected.shapeAppearanceModel = itemBinding.imgFrameSelected.shapeAppearanceModel.toBuilder()
                    .setTopLeftCorner(CornerFamily.ROUNDED,18f)
                    .setBottomLeftCorner(CornerFamily.ROUNDED,18f)
                    .build()

            }else if(adapterPosition%6==5)
            {
                itemBinding.imgFrame.shapeAppearanceModel = itemBinding.imgFrame.shapeAppearanceModel.toBuilder()
                    .setTopRightCorner(CornerFamily.ROUNDED,18f)
                    .setBottomRightCorner(CornerFamily.ROUNDED,18f)
                    .build()
                itemBinding.imgFrameSelected.shapeAppearanceModel = itemBinding.imgFrameSelected.shapeAppearanceModel.toBuilder()
                    .setTopRightCorner(CornerFamily.ROUNDED,18f)
                    .setBottomRightCorner(CornerFamily.ROUNDED,18f)
                    .build()
                itemBinding.imgDownload.shapeAppearanceModel = itemBinding.imgFrame.shapeAppearanceModel.toBuilder()
                    .setTopRightCorner(CornerFamily.ROUNDED,18f)
                    .build()
                val paramsFrame = itemBinding.imgFrame.layoutParams as MarginLayoutParams
                val paramsDownload = itemBinding.imgDownload.layoutParams as MarginLayoutParams
                paramsFrame.rightMargin = 16
                paramsDownload.rightMargin = 16
            }
            if(selectedPosition == adapterPosition)
            {
                itemBinding.imgFrameSelected.visibility = View.VISIBLE
            }else
            {
                itemBinding.imgFrameSelected.visibility = View.GONE
            }
        }
        init {
            itemBinding.root.setOnClickListener()
            {
                adapterListener?.onPhotoFrameClickListener(adapterPosition)
            }
        }
    }
    interface OnPhotoFrameClickListener
    {
        fun onPhotoFrameClickListener(position: Int)
    }
}
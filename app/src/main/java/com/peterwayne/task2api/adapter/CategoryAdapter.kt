package com.peterwayne.task2api.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.peterwayne.task2api.R
import com.peterwayne.task2api.databinding.ItemCategoryBinding
import com.peterwayne.task2api.model.Category

class CategoryAdapter (val adapterListener : OnCategoryClickListener): RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
    var listCategory = listOf<Category>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var selectedPosition = 0
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listCategory[position])
    }

    override fun getItemCount(): Int {
        return listCategory.size
    }
    inner class ViewHolder(private val itemBinding : ItemCategoryBinding) : RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(category: Category)
        {
            itemBinding.txtCategory.text = category.folder
            if(selectedPosition == adapterPosition)
            {
                itemBinding.txtCategory.typeface = ResourcesCompat.getFont(itemBinding.root.context, R.font.cabin_semibold)
                itemBinding.txtCategory.setTextColor(ContextCompat.getColor(itemBinding.root.context, R.color.category_selected))
            }else
            {
                itemBinding.txtCategory.typeface = ResourcesCompat.getFont(itemBinding.root.context, R.font.cabin)
                itemBinding.txtCategory.setTextColor(ContextCompat.getColor(itemBinding.root.context, R.color.category_normal))
            }
        }
        init {
            itemBinding.root.setOnClickListener()
            {
                adapterListener.onCategoryClickListener(adapterPosition)
            }
        }
    }
    interface OnCategoryClickListener
    {
        fun onCategoryClickListener(position: Int)
    }
}
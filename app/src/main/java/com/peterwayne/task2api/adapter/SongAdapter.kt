package com.peterwayne.task2api.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.peterwayne.task2api.databinding.ItemSongBinding
import com.peterwayne.task2api.model.Song

class SongAdapter(val adapterListener: OnSongClickListener) : RecyclerView.Adapter<SongAdapter.ViewHolder>() {
    var songList = listOf<Song>()
    set(value) {
        field = value
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemSongBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(songList[position])
    }

    override fun getItemCount(): Int {
        return songList.size
    }

    inner class ViewHolder(private val itemBinding: ItemSongBinding) : RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(song : Song)
        {
            itemBinding.txtSongTitle.text = song.title
            itemBinding.txtArtist.text = song.artist
        }
        init {
            itemBinding.root.setOnClickListener {
                adapterListener.onSongClick(adapterPosition)
            }
        }
    }
    interface OnSongClickListener
    {
        fun onSongClick(position : Int)
    }
}
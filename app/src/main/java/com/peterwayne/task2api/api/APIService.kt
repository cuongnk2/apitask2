package com.peterwayne.task2api.api

import com.peterwayne.task2api.model.Birthday
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface APIService {
    companion object {
        val apiService:APIService = Retrofit.Builder()
            .baseUrl("https://mystoragetm.s3.ap-southeast-1.amazonaws.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(APIService::class.java)
    }

    @GET("frames_birthday.json")
    fun getBirthdayData(): Call<Birthday>
}
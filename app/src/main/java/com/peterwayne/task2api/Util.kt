package com.peterwayne.task2api

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaMetadataRetriever
import android.os.Environment
import android.util.Log
import java.io.ByteArrayInputStream
import java.io.File
import java.io.InputStream


object Util {
    val pictureDirectory = File(Environment.getExternalStorageDirectory().absolutePath+"/"+Environment.DIRECTORY_PICTURES + "/MyPic")
    fun isFileExisted(fileName : String): Boolean{

        val path = "${pictureDirectory}/${fileName}"
        Log.d("existed", File(path).exists().toString())
        return File(path).exists()
    }

    fun getTimeString(millis : Int): String {
        val buf = StringBuffer()
        buf
            .append(String.format("%02d", (millis % (1000 * 60 * 60) / (1000 * 60))))
            .append(":")
            .append(String.format("%02d", (millis % (1000 * 60 * 60) % (1000 * 60) / 1000)))
        return buf.toString()
    }
    fun songArt(path: String, context: Context): Bitmap {
        val retriever = MediaMetadataRetriever()
        val inputStream: InputStream
        retriever.setDataSource(path)
        if (retriever.embeddedPicture != null) {
            inputStream = ByteArrayInputStream(retriever.embeddedPicture)
            val bitmap = BitmapFactory.decodeStream(inputStream)
            retriever.release()
            return bitmap
        } else {
            return getLargeIcon(context)
        }
    }

    private fun getLargeIcon(context: Context): Bitmap {
        return BitmapFactory.decodeResource(context.resources, R.drawable.headphones)
    }

    init {
        if(!pictureDirectory.exists())
        {
            pictureDirectory.mkdirs()
        }
    }

}
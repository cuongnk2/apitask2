package com.peterwayne.task2api.fragments

import android.Manifest
import android.app.DownloadManager
import android.content.*
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.slider.LabelFormatter
import com.google.android.material.slider.Slider
import com.peterwayne.task2api.R
import com.peterwayne.task2api.Util
import com.peterwayne.task2api.Util.getTimeString
import com.peterwayne.task2api.adapter.CategoryAdapter
import com.peterwayne.task2api.adapter.PhotoFrameAdapter
import com.peterwayne.task2api.adapter.SongAdapter
import com.peterwayne.task2api.databinding.FragmentContentMainBinding
import com.peterwayne.task2api.databinding.LayoutBottomSheetBinding
import com.peterwayne.task2api.model.PhotoFrame
import com.peterwayne.task2api.playback.MusicNotificationManager
import com.peterwayne.task2api.playback.MusicService
import com.peterwayne.task2api.playback.PlaybackInfoListener
import com.peterwayne.task2api.playback.PlayerAdapter
import com.peterwayne.task2api.viewmodel.MainViewModel
import java.io.File


class FragmentContentMain : Fragment(), PhotoFrameAdapter.OnPhotoFrameClickListener,
    CategoryAdapter.OnCategoryClickListener, SongAdapter.OnSongClickListener {
    private lateinit var viewModel: MainViewModel
    private val categoryAdapter = CategoryAdapter(this)
    private val photoFrameAdapter = PhotoFrameAdapter(this)
    private val songAdapter = SongAdapter(this)
    private val categoryLayoutManager =
        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    private val photoFrameLayoutManager =
        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    private val songLayoutManager =
        LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    private lateinit var photoFrame: PhotoFrame
    private lateinit var binding: FragmentContentMainBinding
    private lateinit var bottomSheetBinding: LayoutBottomSheetBinding
    private lateinit var bottomSheetDialog: BottomSheetDialog

    private val onCompletedDownloadReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, intent: Intent?) {
            val id = intent?.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
            if (viewModel.downloadID == id) {
                displayFrame(photoFrame)
                photoFrameAdapter.notifyDataSetChanged()
            }
        }
    }
    private var mMusicService: MusicService? = null
    private var mIsBound: Boolean? = null
    private var mPlayerAdapter: PlayerAdapter? = null
    private var mPlaybackListener: PlaybackListener? = null
    private var mMusicNotificationManager: MusicNotificationManager? = null
    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName?, iBinder: IBinder?) {
            mMusicService = (iBinder as MusicService.LocalBinder).instance
            mPlayerAdapter = mMusicService!!.musicPlayer
            mMusicNotificationManager = mMusicService!!.musicNotificationManager
            if (mPlaybackListener == null) {
                mPlaybackListener = PlaybackListener()
                mPlayerAdapter!!.setPlaybackInfoListener(mPlaybackListener!!)
            }
            if (mPlayerAdapter != null && mPlayerAdapter!!.isPlaying()) {

                restorePlayerStatus()
            }
            checkReadStoragePermissions()
        }
        override fun onServiceDisconnected(p0: ComponentName?) {
            mMusicService = null
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentContentMainBinding.inflate(inflater, container, false)
        requireActivity().registerReceiver(
            onCompletedDownloadReceiver,
            IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        )
        initView()
//        initViewModel()
        addEvent()
        return binding.root
    }

    private fun addEvent() {
        binding.imgNone.setOnClickListener {
            binding.imgFrame.setImageResource(0)
            binding.txtPhotoTitle.text = ""
        }
        binding.btnNext.setOnClickListener(){
            if(mPlayerAdapter!!.isMediaPlayer())
            {
                mPlayerAdapter!!.skip(true)
            }
        }
        binding.btnPrevious.setOnClickListener()
        {
            if(mPlayerAdapter!!.isMediaPlayer())
            {
                mPlayerAdapter!!.instantReset()
            }
        }
        binding.btnPlay.setOnClickListener()
        {
            if(mPlayerAdapter!!.isMediaPlayer())
            {
                mPlayerAdapter!!.resumeOrPause()
            }
        }
        binding.slider.setLabelFormatter(LabelFormatter { value -> getTimeString(value.toInt()) })
        binding.slider.addOnChangeListener(Slider.OnChangeListener { slider, value, fromUser ->
            if (fromUser) {
                mPlayerAdapter!!.seekTo(value.toInt())
            }
        })
        binding.rcvAlbum.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                scrollToRelativeCategory()
            }
        })
        binding.btnList.setOnClickListener()
        {
            bottomSheetDialog.show()
        }
    }

    override fun onCategoryClickListener(position: Int) {
        val category = viewModel.categoryList.value!![position]
        val frame = category.listFrame[0]
        val framePosition = viewModel.photoFrameList.value!!.indexOf(frame)
        binding.rcvAlbum.scrollToPosition(framePosition)
    }

    override fun onPhotoFrameClickListener(position: Int) {
        photoFrame = viewModel.photoFrameList.value!![position]
        photoFrameAdapter.selectedPosition = position
        photoFrameAdapter.notifyDataSetChanged()
        if (Util.isFileExisted(photoFrame.getPhotoName())) {
            displayFrame(photoFrame)
        } else {
            viewModel.downloadImage(requireContext(), photoFrame)
        }
    }

    private fun displayFrame(photoFrame: PhotoFrame) {
        val imgFile = File("${Util.pictureDirectory.absolutePath}/${photoFrame.getPhotoName()}")
        Log.d("img", imgFile.exists().toString())
        if (imgFile.exists()) {
            val myBitmap = BitmapFactory.decodeFile(imgFile.absolutePath)
            binding.imgFrame.setImageBitmap(myBitmap)
        }
        binding.txtPhotoTitle.text = photoFrame.getPhotoName()
    }

    private fun scrollToRelativeCategory() {
        val lastVisibleFrame = photoFrameLayoutManager.findLastVisibleItemPosition()
        if (lastVisibleFrame > -1) {
            val temp: PhotoFrame = viewModel.photoFrameList.value!![lastVisibleFrame]
            viewModel.categoryList.value!!.forEach {
                if (it.folder.equals(temp.folder)) {
                    binding.rcvCategory.smoothScrollToPosition(
                        viewModel.categoryList.value!!.indexOf(it)
                    )
                    categoryAdapter.selectedPosition = (viewModel.categoryList.value!!.indexOf(it))
                }
            }
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
        viewModel.categoryList.observe(viewLifecycleOwner, Observer {
            categoryAdapter.listCategory = it
        })
        viewModel.photoFrameList.observe(viewLifecycleOwner, Observer {
            photoFrameAdapter.photoFrameList = it
        })
        viewModel.songList.observe(viewLifecycleOwner, Observer {
            songAdapter.songList = it
        })
        viewModel.callAPI()
        viewModel.getAllSongs(activity?.contentResolver!!)
    }

    private fun initView() {

        bottomSheetBinding = LayoutBottomSheetBinding.inflate(layoutInflater)
        bottomSheetDialog = BottomSheetDialog(requireContext())
        bottomSheetDialog.setContentView(bottomSheetBinding.root)
        binding.rcvCategory.layoutManager = categoryLayoutManager
        binding.rcvCategory.adapter = categoryAdapter
        binding.rcvAlbum.layoutManager = photoFrameLayoutManager
        binding.rcvAlbum.adapter = photoFrameAdapter
        bottomSheetBinding.rcvSongs.layoutManager = songLayoutManager
        bottomSheetBinding.rcvSongs.adapter = songAdapter
    }

    override fun onSongClick(position: Int) {
        val song = viewModel.songList.value!![position]
        if (binding.slider.isEnabled) {
            binding.slider.isEnabled = true
        }
        try {
            mPlayerAdapter!!.setCurrentSong(song, viewModel.songList.value!!)
            mPlayerAdapter!!.initMediaPlayer()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        bottomSheetDialog.dismiss()
    }


//    private fun updatePlaybackInfo() {
//        if(currentSong!=null && mediaPlayer!=null)
//        {
//            binding.txtSongTitle.text = currentSong!!.title
//            binding.txtArtist.text = currentSong!!.artist
//            binding.txtDuration.text = getTimeString(mediaPlayer!!.duration)
//            binding.slider.valueTo = mediaPlayer!!.duration.toFloat()
//        }
//    }
    override fun onDestroy() {
        super.onDestroy()
        requireActivity().unregisterReceiver(onCompletedDownloadReceiver)
    }

    private fun updatePlayingStatus() {
        val drawable = if (mPlayerAdapter!!.getState() != PlaybackInfoListener.PAUSED)
            R.drawable.ic_pause
        else
            R.drawable.ic_play
        binding.btnPlay.post {
            binding.btnPlay.setImageResource(drawable)
        }
    }

    private fun restorePlayerStatus() {
        binding.slider.isEnabled = mPlayerAdapter!!.isMediaPlayer()
        //if we are playing and the activity was restarted
        //update the controls panel
        if (mPlayerAdapter != null && mPlayerAdapter!!.isMediaPlayer()) {
            mPlayerAdapter!!.onResumeActivity()
            updatePlayingInfo(restore = true, startPlay = false)
        }
    }

    internal inner class PlaybackListener : PlaybackInfoListener() {

        override fun onPositionChanged(position: Int) {
            binding.slider.value = position.toFloat()
        }

        override fun onStateChanged(state: Int) {
            updatePlayingStatus()
            if (mPlayerAdapter!!.getState() != PAUSED
                && mPlayerAdapter!!.getState() != PAUSED
            ) {
                updatePlayingInfo(restore = false, startPlay = true)
            }
        }
        override fun onPlaybackCompleted() {
            //After playback is complete
        }
    }

    private fun updatePlayingInfo(restore : Boolean, startPlay: Boolean) {
        if (startPlay) {
            mPlayerAdapter!!.getMediaPlayer()?.start()
            Handler().postDelayed({
                mMusicService!!.startForeground(
                    MusicNotificationManager.NOTIFICATION_ID,
                    mMusicNotificationManager!!.createNotification())
            }, 200)
        }
        val currentSong = mPlayerAdapter!!.getCurrentSong()
        val duration = mPlayerAdapter!!.getDuration()
        binding.txtSongTitle.text = currentSong?.title
        binding.txtArtist.text = currentSong?.artist
        binding.txtDuration.text = getTimeString(duration.toInt())
        binding.slider.valueTo = duration
        if (restore) {
            binding.slider.value = mPlayerAdapter!!.getPlayerPosition().toFloat()
            updatePlayingStatus()
            Handler().postDelayed({
                //stop foreground if coming from pause state
                if (mMusicService!!.isRestoredFromPause) {
                    mMusicService!!.stopForeground(false)
                    mMusicService!!.musicNotificationManager!!.notificationManager
                        .notify(MusicNotificationManager.NOTIFICATION_ID,
                            mMusicService!!.musicNotificationManager!!.notificationBuilder!!.build())
                    mMusicService!!.isRestoredFromPause = false
                }
            }, 200)
        }
    }
    private fun doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because we want a specific service implementation that
        // we know will be running in our own process (and thus won't be
        // supporting component replacement by other applications).
        val intent = Intent(context, MusicService::class.java)
        requireActivity().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
        mIsBound = true

        val startNotStickyIntent = Intent(context, MusicService::class.java)
        requireActivity().startService(startNotStickyIntent)
    }

    private fun doUnbindService() {
        if (mIsBound!!) {
            // Detach our existing connection.
            requireActivity().unbindService(serviceConnection)
            mIsBound = false
        }
    }
    override fun onResume() {
        super.onResume()
        doBindService()
        if (mPlayerAdapter != null && mPlayerAdapter!!.isPlaying()) {
            restorePlayerStatus()
        }
    }

    override fun onPause() {
        super.onPause()
        doUnbindService()
        if (mPlayerAdapter != null && mPlayerAdapter!!.isMediaPlayer()) {
            mPlayerAdapter!!.onPauseActivity()
        }
    }
    private fun checkReadStoragePermissions() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
        }else initViewModel()
    }


    @Deprecated("Deprecated in Java")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            requireActivity().finish()
        }else initViewModel()
    }

}